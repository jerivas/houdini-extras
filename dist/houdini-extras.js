(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("Houdini"));
	else if(typeof define === 'function' && define.amd)
		define(["Houdini"], factory);
	else if(typeof exports === 'object')
		exports["houdiniExtras"] = factory(require("Houdini"));
	else
		root["houdiniExtras"] = factory(root["houdini"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_0__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 5);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_0__;

/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export expand */
/* unused harmony export collapse */
/* harmony export (immutable) */ __webpack_exports__["a"] = initAnimations;
/**
* Instantiate and dispatch a custom event
* @param  {String} name Name of the custom event
* @param  {Element} target DOM element that will be available as event.target
* @param  {Object} detail Object that will be available as event.detail
*/
function dispatchEvent(name, target, detail) {
	// IE doesn't support CustomEvent
	if (typeof window.CustomEvent !== 'function') return;
	var event = new CustomEvent(name, {
		detail: detail,
		bubbles: true,
		cancelable: true
	});
	target.dispatchEvent(event);
}

/**
 * Add height transition support when **expanding** Houdini elements.
 * Transitions should be handled by CSS, the function will just give the content
 * concrete height values since transitioning into height: auto is not possible.
 * Make sure `content` doesn't receive Houdini's standard 'collapse' class, since it will
 * set many styles that will break height transitions.
 *
 * @param {HTMLElement} content Houdini content that will receive concrete heights
 * @param {HTMLElement} [toggle] Optional Houdini toggle element associated to the content
 * @see {@link https://css-tricks.com/using-css-transitions-auto-dimensions/#article-header-id-5}
 */
function expand(content, toggle) {
	var targetHeight = content.scrollHeight;
	content.style.height = targetHeight + 'px';

	content.addEventListener('transitionend', function transitionHandler() {
		// Remove the transition listener so it only gets triggered once
		content.removeEventListener('transitionend', transitionHandler);
		// Re-enable the regular content flow by removing the concrete height
		content.style.removeProperty('height');
		dispatchEvent('openedAnimation.houdini', content, { toggle: toggle });
	});
}

/**
 * Add height transition support when **collapsing** Houdini elements.
 * Transitions should be handled by CSS, the function will just give the content
 * concrete height values since transitioning from height: auto is not possible.
 * Make sure `content` doesn't receive Houdini's standard 'collapse' class, since it will
 * set many styles that will break height transitions.
 *
 * @param {HTMLElement} content Houdini content that will receive concrete heights
 * @param {HTMLElement} [toggle] Optional Houdini toggle element associated to the content
 * @see {@link https://css-tricks.com/using-css-transitions-auto-dimensions/#article-header-id-5}
 */
function collapse(content, toggle) {
	var targetHeight = content.scrollHeight;
	var contentTransition = content.style.transition;
	content.style.transition = '';

	// On the next frame (as soon as the previous style change has taken effect),
	// explicitly set the content's height to its current pixel height, so we
	// aren't transitioning out of 'auto'
	requestAnimationFrame(function () {
		content.style.height = targetHeight + 'px';
		content.style.transition = contentTransition;

		// Finally transition to height: 0
		requestAnimationFrame(function () {
			content.style.height = '0px';
			dispatchEvent('closedAnimation.houdini', content, { toggle: toggle });
		});
	});
}

/**
 * Add event listeners to animate Houdini expand/collapse actions
 * @param  {String} options.contentClass Class of the animated Houdini collapsibles
 * @param  {String} options.activeClass  Class that indicates a collapsible is expanded
 */
function initAnimations() {
	var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
	    _ref$contentClass = _ref.contentClass,
	    contentClass = _ref$contentClass === undefined ? 'collapse-animated' : _ref$contentClass,
	    _ref$activeClass = _ref.activeClass,
	    activeClass = _ref$activeClass === undefined ? 'active' : _ref$activeClass;

	// Notice we are attaching to the events fired before Houdini does the class switching
	document.addEventListener('open.houdini', function (event) {
		var content = event.target;
		if (content.classList.contains(contentClass)) {
			expand(content, event.detail.toggle);
		}
	});

	document.addEventListener('close.houdini', function (event) {
		var classList = event.target.classList;
		if (classList.contains(contentClass) && classList.contains(activeClass)) {
			collapse(event.target, event.detail.toggle);
		}
	});

	// Collapse all contents not marked as initially active
	// This gives us a consistent animation and DOM state
	var animatedContents = document.querySelectorAll('.' + contentClass);
	[].forEach.call(animatedContents, function (content) {
		if (!content.classList.contains(activeClass)) collapse(content);
	});
}

/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_Houdini__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_Houdini___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_Houdini__);
/* harmony export (immutable) */ __webpack_exports__["a"] = initCloseButtons;


/**
 * Attach event listeners to close Houdini contents.
 * Compatible with any element with a data-close attribute with an ID as value.
 * @example <button data-close="#id-of-content">Close</button>
 */
function initCloseButtons() {
	document.addEventListener('click', function (event) {
		var targetId = event.target.dataset.close;
		if (targetId) {
			var toggle = document.querySelector('[href="' + targetId + '"]');
			__WEBPACK_IMPORTED_MODULE_0_Houdini___default.a.closeContent(targetId, toggle);
		}
	});
}

/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_Houdini__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_Houdini___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_Houdini__);
/* harmony export (immutable) */ __webpack_exports__["a"] = initModals;


/**
 * Attach event listeners to create Houdini-based modals.
 *
 * @param {String} options.modalClass  Class name that identifies all modals
 * @param {String} options.activeClass Class name that indicates a modal is a active
 */
function initModals() {
	var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
	    _ref$modalClass = _ref.modalClass,
	    modalClass = _ref$modalClass === undefined ? 'houdini-modal' : _ref$modalClass,
	    _ref$activeClass = _ref.activeClass,
	    activeClass = _ref$activeClass === undefined ? 'active' : _ref$activeClass;

	var body = document.querySelector('body');
	var isModal = function isModal(el) {
		return el.classList.contains(modalClass);
	};

	// When a modal is opened...
	document.addEventListener('opened.houdini', function (event) {
		// Remove the <body> scrollbar
		if (isModal(event.target)) body.style.overflowY = 'hidden';
	});

	// When a modal is closed...
	document.addEventListener('closed.houdini', function (event) {
		if (isModal(event.target)) {
			// Return the <body> to its normal scrollbar state
			body.style.removeProperty('overflow-y');
			// Give focus back to the toggle that triggered the modal
			if (event.detail.toggle) event.detail.toggle.focus();
		}
	});

	// Close the modal when the modal overlay is clicked
	document.addEventListener('click', function (event) {
		var content = event.target;

		if (isModal(content)) {
			var toggleEl = document.querySelector('[href="#' + content.id + '"]');
			__WEBPACK_IMPORTED_MODULE_0_Houdini___default.a.closeContent(content.id, toggleEl);
		}
	});

	// Close the active modal on ESC
	document.addEventListener('keydown', function (e) {
		if (e.keyCode === 27) {
			var openModal = document.querySelector('.' + modalClass + '.' + activeClass);
			if (openModal) {
				var toggleEl = document.querySelector('[href="#' + openModal.id + '"]');
				__WEBPACK_IMPORTED_MODULE_0_Houdini___default.a.closeContent(openModal.id, toggleEl);
			}
		}
	});
}

/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_Houdini__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_Houdini___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_Houdini__);
/* unused harmony export getEmbedCode */
/* harmony export (immutable) */ __webpack_exports__["a"] = initVideos;


/**
 * Generate a YouTube or Vimeo embed code from a URL.
 * The resulting iframe code will have autoplay enabled.
 * @param  {String} url The url of a YouTube or Vimeo video
 * @return {String}     The embed code for YouTube or Vimeo
 */
function getEmbedCode(url) {
	var vimeoRE = /(?:https?:\/\/)?(?:www\.)?(?:vimeo\.com)\/([^/?]+)/g;
	var youtubeRE = /(?:https?:\/\/)?(?:www\.)?(?:youtube\.com|youtu\.be)\/(?:watch\?v=)?([^/&]+)/g;
	var match = void 0;

	match = vimeoRE.exec(url);
	if (match) {
		return '<iframe width="840" height="470" allowfullscreen\n\t\t\tsrc="https://player.vimeo.com/video/' + match[1] + '?autoplay=1"></iframe>';
	}

	match = youtubeRE.exec(url);
	if (match) {
		return '<iframe width="840" height="470" allowfullscreen\n\t\t\tsrc="https://www.youtube.com/embed/' + match[1] + '?autoplay=1"></iframe>';
	}

	return '';
}

/**
 * Attach event listeners required to create and show video modals.
 *
 * @param  {String} options.modalClass     Class name applied to all modals
 * @param  {String} options.videoClass     Class name applied to all video modals
 * @param  {String} options.buttonSelector Selector of all elements that fire video modals
 * @param  {String} options.modalTemplate
 *         Video modal markup. `{{iframe}}` will be replaced with the actual embed code.
 */
function initVideos() {
	var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
	    _ref$modalClass = _ref.modalClass,
	    modalClass = _ref$modalClass === undefined ? 'houdini-modal' : _ref$modalClass,
	    _ref$videoClass = _ref.videoClass,
	    videoClass = _ref$videoClass === undefined ? 'houdini-video-modal' : _ref$videoClass,
	    _ref$buttonSelector = _ref.buttonSelector,
	    buttonSelector = _ref$buttonSelector === undefined ? '[data-video]' : _ref$buttonSelector,
	    _ref$modalTemplate = _ref.modalTemplate,
	    modalTemplate = _ref$modalTemplate === undefined ? '<div class="houdini-modal houdini-video-modal">{{iframe}}</div>' : _ref$modalTemplate;

	var videoBtns = document.querySelectorAll(buttonSelector);
	var videoModal = void 0;

	// Open video modals when video buttons are clicked
	[].forEach.call(videoBtns, function (videoBtn) {
		videoBtn.addEventListener('click', function (event) {
			event.preventDefault();
			var embedCode = getEmbedCode(videoBtn.href);
			var tempNode = document.createElement('div');
			tempNode.innerHTML = modalTemplate.replace('{{iframe}}', embedCode);
			videoModal = tempNode.firstElementChild;
			videoModal.id = 'houdini-video-modal';
			document.body.appendChild(videoModal);
			setTimeout(function () {
				__WEBPACK_IMPORTED_MODULE_0_Houdini___default.a.openContent(videoModal.id, videoBtn);
			}, 10);
		});
	});

	// Delete the videoModal node when closed
	document.addEventListener('closed.houdini', function (event) {
		var classes = event.target.classList;

		if (classes.contains(modalClass) && classes.contains(videoClass)) {
			videoModal = event.target;
			videoModal.parentNode.removeChild(videoModal);
		}
	});
}

/***/ }),
/* 5 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__animations__ = __webpack_require__(1);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "initAnimations", function() { return __WEBPACK_IMPORTED_MODULE_0__animations__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__close_buttons__ = __webpack_require__(2);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "initCloseButtons", function() { return __WEBPACK_IMPORTED_MODULE_1__close_buttons__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__modals__ = __webpack_require__(3);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "initModals", function() { return __WEBPACK_IMPORTED_MODULE_2__modals__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__videos__ = __webpack_require__(4);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "initVideos", function() { return __WEBPACK_IMPORTED_MODULE_3__videos__["a"]; });





/***/ })
/******/ ]);
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay91bml2ZXJzYWxNb2R1bGVEZWZpbml0aW9uIiwid2VicGFjazovLy93ZWJwYWNrL2Jvb3RzdHJhcCBkYmNiMjNlNmM0YTMxYmU4ZGU0ZiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwge1wiY29tbW9uanNcIjpcIkhvdWRpbmlcIixcImNvbW1vbmpzMlwiOlwiSG91ZGluaVwiLFwiYW1kXCI6XCJIb3VkaW5pXCIsXCJyb290XCI6XCJob3VkaW5pXCJ9Iiwid2VicGFjazovLy8uL3NyYy9hbmltYXRpb25zLmpzIiwid2VicGFjazovLy8uL3NyYy9jbG9zZS1idXR0b25zLmpzIiwid2VicGFjazovLy8uL3NyYy9tb2RhbHMuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3ZpZGVvcy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvaW5kZXguanMiXSwibmFtZXMiOlsiZGlzcGF0Y2hFdmVudCIsIm5hbWUiLCJ0YXJnZXQiLCJkZXRhaWwiLCJ3aW5kb3ciLCJDdXN0b21FdmVudCIsImV2ZW50IiwiYnViYmxlcyIsImNhbmNlbGFibGUiLCJleHBhbmQiLCJjb250ZW50IiwidG9nZ2xlIiwidGFyZ2V0SGVpZ2h0Iiwic2Nyb2xsSGVpZ2h0Iiwic3R5bGUiLCJoZWlnaHQiLCJhZGRFdmVudExpc3RlbmVyIiwidHJhbnNpdGlvbkhhbmRsZXIiLCJyZW1vdmVFdmVudExpc3RlbmVyIiwicmVtb3ZlUHJvcGVydHkiLCJjb2xsYXBzZSIsImNvbnRlbnRUcmFuc2l0aW9uIiwidHJhbnNpdGlvbiIsInJlcXVlc3RBbmltYXRpb25GcmFtZSIsImluaXRBbmltYXRpb25zIiwiY29udGVudENsYXNzIiwiYWN0aXZlQ2xhc3MiLCJkb2N1bWVudCIsImNsYXNzTGlzdCIsImNvbnRhaW5zIiwiYW5pbWF0ZWRDb250ZW50cyIsInF1ZXJ5U2VsZWN0b3JBbGwiLCJmb3JFYWNoIiwiY2FsbCIsImluaXRDbG9zZUJ1dHRvbnMiLCJ0YXJnZXRJZCIsImRhdGFzZXQiLCJjbG9zZSIsInF1ZXJ5U2VsZWN0b3IiLCJob3VkaW5pIiwiY2xvc2VDb250ZW50IiwiaW5pdE1vZGFscyIsIm1vZGFsQ2xhc3MiLCJib2R5IiwiaXNNb2RhbCIsImVsIiwib3ZlcmZsb3dZIiwiZm9jdXMiLCJ0b2dnbGVFbCIsImlkIiwiZSIsImtleUNvZGUiLCJvcGVuTW9kYWwiLCJnZXRFbWJlZENvZGUiLCJ1cmwiLCJ2aW1lb1JFIiwieW91dHViZVJFIiwibWF0Y2giLCJleGVjIiwiaW5pdFZpZGVvcyIsInZpZGVvQ2xhc3MiLCJidXR0b25TZWxlY3RvciIsIm1vZGFsVGVtcGxhdGUiLCJ2aWRlb0J0bnMiLCJ2aWRlb01vZGFsIiwidmlkZW9CdG4iLCJwcmV2ZW50RGVmYXVsdCIsImVtYmVkQ29kZSIsImhyZWYiLCJ0ZW1wTm9kZSIsImNyZWF0ZUVsZW1lbnQiLCJpbm5lckhUTUwiLCJyZXBsYWNlIiwiZmlyc3RFbGVtZW50Q2hpbGQiLCJhcHBlbmRDaGlsZCIsInNldFRpbWVvdXQiLCJvcGVuQ29udGVudCIsImNsYXNzZXMiLCJwYXJlbnROb2RlIiwicmVtb3ZlQ2hpbGQiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRCxPO0FDVkE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0EsbURBQTJDLGNBQWM7O0FBRXpEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7O0FBRUE7QUFDQTs7Ozs7OztBQ2hFQSwrQzs7Ozs7Ozs7O0FDQUM7QUFBQTs7Ozs7O0FBTUQsU0FBU0EsYUFBVCxDQUF1QkMsSUFBdkIsRUFBNkJDLE1BQTdCLEVBQXFDQyxNQUFyQyxFQUE2QztBQUM1QztBQUNBLEtBQUksT0FBT0MsT0FBT0MsV0FBZCxLQUE4QixVQUFsQyxFQUE4QztBQUM5QyxLQUFNQyxRQUFRLElBQUlELFdBQUosQ0FBZ0JKLElBQWhCLEVBQXNCO0FBQ25DRSxnQkFEbUM7QUFFbkNJLFdBQVMsSUFGMEI7QUFHbkNDLGNBQVk7QUFIdUIsRUFBdEIsQ0FBZDtBQUtBTixRQUFPRixhQUFQLENBQXFCTSxLQUFyQjtBQUNBOztBQUVEOzs7Ozs7Ozs7OztBQVdPLFNBQVNHLE1BQVQsQ0FBZ0JDLE9BQWhCLEVBQXlCQyxNQUF6QixFQUFpQztBQUN2QyxLQUFNQyxlQUFlRixRQUFRRyxZQUE3QjtBQUNBSCxTQUFRSSxLQUFSLENBQWNDLE1BQWQsR0FBMEJILFlBQTFCOztBQUVBRixTQUFRTSxnQkFBUixDQUF5QixlQUF6QixFQUEwQyxTQUFTQyxpQkFBVCxHQUE2QjtBQUN0RTtBQUNBUCxVQUFRUSxtQkFBUixDQUE0QixlQUE1QixFQUE2Q0QsaUJBQTdDO0FBQ0E7QUFDQVAsVUFBUUksS0FBUixDQUFjSyxjQUFkLENBQTZCLFFBQTdCO0FBQ0FuQixnQkFBYyx5QkFBZCxFQUF5Q1UsT0FBekMsRUFBa0QsRUFBRUMsY0FBRixFQUFsRDtBQUNBLEVBTkQ7QUFPQTs7QUFFRDs7Ozs7Ozs7Ozs7QUFXTyxTQUFTUyxRQUFULENBQWtCVixPQUFsQixFQUEyQkMsTUFBM0IsRUFBbUM7QUFDekMsS0FBTUMsZUFBZUYsUUFBUUcsWUFBN0I7QUFDQSxLQUFNUSxvQkFBb0JYLFFBQVFJLEtBQVIsQ0FBY1EsVUFBeEM7QUFDQVosU0FBUUksS0FBUixDQUFjUSxVQUFkLEdBQTJCLEVBQTNCOztBQUVBO0FBQ0E7QUFDQTtBQUNBQyx1QkFBc0IsWUFBTTtBQUMzQmIsVUFBUUksS0FBUixDQUFjQyxNQUFkLEdBQTBCSCxZQUExQjtBQUNBRixVQUFRSSxLQUFSLENBQWNRLFVBQWQsR0FBMkJELGlCQUEzQjs7QUFFQTtBQUNBRSx3QkFBc0IsWUFBTTtBQUMzQmIsV0FBUUksS0FBUixDQUFjQyxNQUFkLEdBQXVCLEtBQXZCO0FBQ0FmLGlCQUFjLHlCQUFkLEVBQXlDVSxPQUF6QyxFQUFrRCxFQUFFQyxjQUFGLEVBQWxEO0FBQ0EsR0FIRDtBQUlBLEVBVEQ7QUFVQTs7QUFFRDs7Ozs7QUFLZSxTQUFTYSxjQUFULEdBR1A7QUFBQSxnRkFBSixFQUFJO0FBQUEsOEJBRlBDLFlBRU87QUFBQSxLQUZQQSxZQUVPLHFDQUZRLG1CQUVSO0FBQUEsNkJBRFBDLFdBQ087QUFBQSxLQURQQSxXQUNPLG9DQURPLFFBQ1A7O0FBQ1A7QUFDQUMsVUFBU1gsZ0JBQVQsQ0FBMEIsY0FBMUIsRUFBMEMsaUJBQVM7QUFDbEQsTUFBTU4sVUFBVUosTUFBTUosTUFBdEI7QUFDQSxNQUFJUSxRQUFRa0IsU0FBUixDQUFrQkMsUUFBbEIsQ0FBMkJKLFlBQTNCLENBQUosRUFBOEM7QUFDN0NoQixVQUFPQyxPQUFQLEVBQWdCSixNQUFNSCxNQUFOLENBQWFRLE1BQTdCO0FBQ0E7QUFDRCxFQUxEOztBQU9BZ0IsVUFBU1gsZ0JBQVQsQ0FBMEIsZUFBMUIsRUFBMkMsaUJBQVM7QUFDbkQsTUFBTVksWUFBWXRCLE1BQU1KLE1BQU4sQ0FBYTBCLFNBQS9CO0FBQ0EsTUFBSUEsVUFBVUMsUUFBVixDQUFtQkosWUFBbkIsS0FBb0NHLFVBQVVDLFFBQVYsQ0FBbUJILFdBQW5CLENBQXhDLEVBQXlFO0FBQ3hFTixZQUFTZCxNQUFNSixNQUFmLEVBQXVCSSxNQUFNSCxNQUFOLENBQWFRLE1BQXBDO0FBQ0E7QUFDRCxFQUxEOztBQU9BO0FBQ0E7QUFDQSxLQUFNbUIsbUJBQW1CSCxTQUFTSSxnQkFBVCxPQUE4Qk4sWUFBOUIsQ0FBekI7QUFDQSxJQUFHTyxPQUFILENBQVdDLElBQVgsQ0FBZ0JILGdCQUFoQixFQUFrQyxtQkFBVztBQUM1QyxNQUFJLENBQUNwQixRQUFRa0IsU0FBUixDQUFrQkMsUUFBbEIsQ0FBMkJILFdBQTNCLENBQUwsRUFBOENOLFNBQVNWLE9BQVQ7QUFDOUMsRUFGRDtBQUdBLEM7Ozs7Ozs7Ozs7QUN0R0Q7O0FBRUE7Ozs7O0FBS2UsU0FBU3dCLGdCQUFULEdBQTRCO0FBQzFDUCxVQUFTWCxnQkFBVCxDQUEwQixPQUExQixFQUFtQyxpQkFBUztBQUMzQyxNQUFNbUIsV0FBVzdCLE1BQU1KLE1BQU4sQ0FBYWtDLE9BQWIsQ0FBcUJDLEtBQXRDO0FBQ0EsTUFBSUYsUUFBSixFQUFjO0FBQ2IsT0FBTXhCLFNBQVNnQixTQUFTVyxhQUFULGFBQWlDSCxRQUFqQyxRQUFmO0FBQ0FJLEdBQUEsK0NBQUFBLENBQVFDLFlBQVIsQ0FBcUJMLFFBQXJCLEVBQStCeEIsTUFBL0I7QUFDQTtBQUNELEVBTkQ7QUFPQSxDOzs7Ozs7Ozs7O0FDZkQ7O0FBRUE7Ozs7OztBQU1lLFNBQVM4QixVQUFULEdBR1A7QUFBQSxnRkFBSixFQUFJO0FBQUEsNEJBRlBDLFVBRU87QUFBQSxLQUZQQSxVQUVPLG1DQUZNLGVBRU47QUFBQSw2QkFEUGhCLFdBQ087QUFBQSxLQURQQSxXQUNPLG9DQURPLFFBQ1A7O0FBQ1AsS0FBTWlCLE9BQU9oQixTQUFTVyxhQUFULENBQXVCLE1BQXZCLENBQWI7QUFDQSxLQUFNTSxVQUFVLFNBQVZBLE9BQVU7QUFBQSxTQUFNQyxHQUFHakIsU0FBSCxDQUFhQyxRQUFiLENBQXNCYSxVQUF0QixDQUFOO0FBQUEsRUFBaEI7O0FBRUE7QUFDQWYsVUFBU1gsZ0JBQVQsQ0FBMEIsZ0JBQTFCLEVBQTRDLGlCQUFTO0FBQ3BEO0FBQ0EsTUFBSTRCLFFBQVF0QyxNQUFNSixNQUFkLENBQUosRUFBMkJ5QyxLQUFLN0IsS0FBTCxDQUFXZ0MsU0FBWCxHQUF1QixRQUF2QjtBQUMzQixFQUhEOztBQUtBO0FBQ0FuQixVQUFTWCxnQkFBVCxDQUEwQixnQkFBMUIsRUFBNEMsaUJBQVM7QUFDcEQsTUFBSTRCLFFBQVF0QyxNQUFNSixNQUFkLENBQUosRUFBMkI7QUFDMUI7QUFDQXlDLFFBQUs3QixLQUFMLENBQVdLLGNBQVgsQ0FBMEIsWUFBMUI7QUFDQTtBQUNBLE9BQUliLE1BQU1ILE1BQU4sQ0FBYVEsTUFBakIsRUFBeUJMLE1BQU1ILE1BQU4sQ0FBYVEsTUFBYixDQUFvQm9DLEtBQXBCO0FBQ3pCO0FBQ0QsRUFQRDs7QUFTQTtBQUNBcEIsVUFBU1gsZ0JBQVQsQ0FBMEIsT0FBMUIsRUFBbUMsaUJBQVM7QUFDM0MsTUFBTU4sVUFBVUosTUFBTUosTUFBdEI7O0FBRUEsTUFBSTBDLFFBQVFsQyxPQUFSLENBQUosRUFBc0I7QUFDckIsT0FBTXNDLFdBQVdyQixTQUFTVyxhQUFULGNBQWtDNUIsUUFBUXVDLEVBQTFDLFFBQWpCO0FBQ0FWLEdBQUEsK0NBQUFBLENBQVFDLFlBQVIsQ0FBcUI5QixRQUFRdUMsRUFBN0IsRUFBaUNELFFBQWpDO0FBQ0E7QUFDRCxFQVBEOztBQVNBO0FBQ0FyQixVQUFTWCxnQkFBVCxDQUEwQixTQUExQixFQUFxQyxhQUFLO0FBQ3pDLE1BQUlrQyxFQUFFQyxPQUFGLEtBQWMsRUFBbEIsRUFBc0I7QUFDckIsT0FBTUMsWUFBWXpCLFNBQVNXLGFBQVQsT0FBMkJJLFVBQTNCLFNBQXlDaEIsV0FBekMsQ0FBbEI7QUFDQSxPQUFJMEIsU0FBSixFQUFlO0FBQ2QsUUFBTUosV0FBV3JCLFNBQVNXLGFBQVQsY0FBa0NjLFVBQVVILEVBQTVDLFFBQWpCO0FBQ0FWLElBQUEsK0NBQUFBLENBQVFDLFlBQVIsQ0FBcUJZLFVBQVVILEVBQS9CLEVBQW1DRCxRQUFuQztBQUNBO0FBQ0Q7QUFDRCxFQVJEO0FBU0EsQzs7Ozs7Ozs7Ozs7QUNuREQ7O0FBRUE7Ozs7OztBQU1PLFNBQVNLLFlBQVQsQ0FBc0JDLEdBQXRCLEVBQTJCO0FBQ2pDLEtBQU1DLFVBQVUscURBQWhCO0FBQ0EsS0FBTUMsWUFBWSwrRUFBbEI7QUFDQSxLQUFJQyxjQUFKOztBQUVBQSxTQUFRRixRQUFRRyxJQUFSLENBQWFKLEdBQWIsQ0FBUjtBQUNBLEtBQUlHLEtBQUosRUFBVztBQUNWLDBHQUN1Q0EsTUFBTSxDQUFOLENBRHZDO0FBRUE7O0FBRURBLFNBQVFELFVBQVVFLElBQVYsQ0FBZUosR0FBZixDQUFSO0FBQ0EsS0FBSUcsS0FBSixFQUFXO0FBQ1YseUdBQ3NDQSxNQUFNLENBQU4sQ0FEdEM7QUFFQTs7QUFFRCxRQUFPLEVBQVA7QUFDQTs7QUFFRDs7Ozs7Ozs7O0FBU2UsU0FBU0UsVUFBVCxHQUtQO0FBQUEsZ0ZBQUosRUFBSTtBQUFBLDRCQUpQakIsVUFJTztBQUFBLEtBSlBBLFVBSU8sbUNBSk0sZUFJTjtBQUFBLDRCQUhQa0IsVUFHTztBQUFBLEtBSFBBLFVBR08sbUNBSE0scUJBR047QUFBQSxnQ0FGUEMsY0FFTztBQUFBLEtBRlBBLGNBRU8sdUNBRlUsY0FFVjtBQUFBLCtCQURQQyxhQUNPO0FBQUEsS0FEUEEsYUFDTyxzQ0FEUyxpRUFDVDs7QUFDUCxLQUFNQyxZQUFZcEMsU0FBU0ksZ0JBQVQsQ0FBMEI4QixjQUExQixDQUFsQjtBQUNBLEtBQUlHLG1CQUFKOztBQUVBO0FBQ0EsSUFBR2hDLE9BQUgsQ0FBV0MsSUFBWCxDQUFnQjhCLFNBQWhCLEVBQTJCLG9CQUFZO0FBQ3RDRSxXQUFTakQsZ0JBQVQsQ0FBMEIsT0FBMUIsRUFBbUMsaUJBQVM7QUFDM0NWLFNBQU00RCxjQUFOO0FBQ0EsT0FBTUMsWUFBWWQsYUFBYVksU0FBU0csSUFBdEIsQ0FBbEI7QUFDQSxPQUFNQyxXQUFXMUMsU0FBUzJDLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBakI7QUFDQUQsWUFBU0UsU0FBVCxHQUFxQlQsY0FBY1UsT0FBZCxDQUFzQixZQUF0QixFQUFvQ0wsU0FBcEMsQ0FBckI7QUFDQUgsZ0JBQWFLLFNBQVNJLGlCQUF0QjtBQUNBVCxjQUFXZixFQUFYLEdBQWdCLHFCQUFoQjtBQUNBdEIsWUFBU2dCLElBQVQsQ0FBYytCLFdBQWQsQ0FBMEJWLFVBQTFCO0FBQ0FXLGNBQVcsWUFBTTtBQUNoQnBDLElBQUEsK0NBQUFBLENBQVFxQyxXQUFSLENBQW9CWixXQUFXZixFQUEvQixFQUFtQ2dCLFFBQW5DO0FBQ0EsSUFGRCxFQUVHLEVBRkg7QUFHQSxHQVhEO0FBWUEsRUFiRDs7QUFlQTtBQUNBdEMsVUFBU1gsZ0JBQVQsQ0FBMEIsZ0JBQTFCLEVBQTRDLGlCQUFTO0FBQ3BELE1BQU02RCxVQUFVdkUsTUFBTUosTUFBTixDQUFhMEIsU0FBN0I7O0FBRUEsTUFBSWlELFFBQVFoRCxRQUFSLENBQWlCYSxVQUFqQixLQUFnQ21DLFFBQVFoRCxRQUFSLENBQWlCK0IsVUFBakIsQ0FBcEMsRUFBa0U7QUFDakVJLGdCQUFhMUQsTUFBTUosTUFBbkI7QUFDQThELGNBQVdjLFVBQVgsQ0FBc0JDLFdBQXRCLENBQWtDZixVQUFsQztBQUNBO0FBQ0QsRUFQRDtBQVFBLEM7Ozs7Ozs7Ozs7Ozs7Ozs7QUN2RUQ7QUFDQTtBQUNBIiwiZmlsZSI6ImhvdWRpbmktZXh0cmFzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIHdlYnBhY2tVbml2ZXJzYWxNb2R1bGVEZWZpbml0aW9uKHJvb3QsIGZhY3RvcnkpIHtcblx0aWYodHlwZW9mIGV4cG9ydHMgPT09ICdvYmplY3QnICYmIHR5cGVvZiBtb2R1bGUgPT09ICdvYmplY3QnKVxuXHRcdG1vZHVsZS5leHBvcnRzID0gZmFjdG9yeShyZXF1aXJlKFwiSG91ZGluaVwiKSk7XG5cdGVsc2UgaWYodHlwZW9mIGRlZmluZSA9PT0gJ2Z1bmN0aW9uJyAmJiBkZWZpbmUuYW1kKVxuXHRcdGRlZmluZShbXCJIb3VkaW5pXCJdLCBmYWN0b3J5KTtcblx0ZWxzZSBpZih0eXBlb2YgZXhwb3J0cyA9PT0gJ29iamVjdCcpXG5cdFx0ZXhwb3J0c1tcImhvdWRpbmlFeHRyYXNcIl0gPSBmYWN0b3J5KHJlcXVpcmUoXCJIb3VkaW5pXCIpKTtcblx0ZWxzZVxuXHRcdHJvb3RbXCJob3VkaW5pRXh0cmFzXCJdID0gZmFjdG9yeShyb290W1wiaG91ZGluaVwiXSk7XG59KSh0aGlzLCBmdW5jdGlvbihfX1dFQlBBQ0tfRVhURVJOQUxfTU9EVUxFXzBfXykge1xucmV0dXJuIFxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyB3ZWJwYWNrL3VuaXZlcnNhbE1vZHVsZURlZmluaXRpb24iLCIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSlcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcblxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gaWRlbnRpdHkgZnVuY3Rpb24gZm9yIGNhbGxpbmcgaGFybW9ueSBpbXBvcnRzIHdpdGggdGhlIGNvcnJlY3QgY29udGV4dFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5pID0gZnVuY3Rpb24odmFsdWUpIHsgcmV0dXJuIHZhbHVlOyB9O1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHtcbiBcdFx0XHRcdGNvbmZpZ3VyYWJsZTogZmFsc2UsXG4gXHRcdFx0XHRlbnVtZXJhYmxlOiB0cnVlLFxuIFx0XHRcdFx0Z2V0OiBnZXR0ZXJcbiBcdFx0XHR9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSA1KTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyB3ZWJwYWNrL2Jvb3RzdHJhcCBkYmNiMjNlNmM0YTMxYmU4ZGU0ZiIsIm1vZHVsZS5leHBvcnRzID0gX19XRUJQQUNLX0VYVEVSTkFMX01PRFVMRV8wX187XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gZXh0ZXJuYWwge1wiY29tbW9uanNcIjpcIkhvdWRpbmlcIixcImNvbW1vbmpzMlwiOlwiSG91ZGluaVwiLFwiYW1kXCI6XCJIb3VkaW5pXCIsXCJyb290XCI6XCJob3VkaW5pXCJ9XG4vLyBtb2R1bGUgaWQgPSAwXG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsIiAvKipcbiAqIEluc3RhbnRpYXRlIGFuZCBkaXNwYXRjaCBhIGN1c3RvbSBldmVudFxuICogQHBhcmFtICB7U3RyaW5nfSBuYW1lIE5hbWUgb2YgdGhlIGN1c3RvbSBldmVudFxuICogQHBhcmFtICB7RWxlbWVudH0gdGFyZ2V0IERPTSBlbGVtZW50IHRoYXQgd2lsbCBiZSBhdmFpbGFibGUgYXMgZXZlbnQudGFyZ2V0XG4gKiBAcGFyYW0gIHtPYmplY3R9IGRldGFpbCBPYmplY3QgdGhhdCB3aWxsIGJlIGF2YWlsYWJsZSBhcyBldmVudC5kZXRhaWxcbiAqL1xuZnVuY3Rpb24gZGlzcGF0Y2hFdmVudChuYW1lLCB0YXJnZXQsIGRldGFpbCkge1xuXHQvLyBJRSBkb2Vzbid0IHN1cHBvcnQgQ3VzdG9tRXZlbnRcblx0aWYgKHR5cGVvZiB3aW5kb3cuQ3VzdG9tRXZlbnQgIT09ICdmdW5jdGlvbicpIHJldHVybjtcblx0Y29uc3QgZXZlbnQgPSBuZXcgQ3VzdG9tRXZlbnQobmFtZSwge1xuXHRcdGRldGFpbCxcblx0XHRidWJibGVzOiB0cnVlLFxuXHRcdGNhbmNlbGFibGU6IHRydWUsXG5cdH0pO1xuXHR0YXJnZXQuZGlzcGF0Y2hFdmVudChldmVudCk7XG59XG5cbi8qKlxuICogQWRkIGhlaWdodCB0cmFuc2l0aW9uIHN1cHBvcnQgd2hlbiAqKmV4cGFuZGluZyoqIEhvdWRpbmkgZWxlbWVudHMuXG4gKiBUcmFuc2l0aW9ucyBzaG91bGQgYmUgaGFuZGxlZCBieSBDU1MsIHRoZSBmdW5jdGlvbiB3aWxsIGp1c3QgZ2l2ZSB0aGUgY29udGVudFxuICogY29uY3JldGUgaGVpZ2h0IHZhbHVlcyBzaW5jZSB0cmFuc2l0aW9uaW5nIGludG8gaGVpZ2h0OiBhdXRvIGlzIG5vdCBwb3NzaWJsZS5cbiAqIE1ha2Ugc3VyZSBgY29udGVudGAgZG9lc24ndCByZWNlaXZlIEhvdWRpbmkncyBzdGFuZGFyZCAnY29sbGFwc2UnIGNsYXNzLCBzaW5jZSBpdCB3aWxsXG4gKiBzZXQgbWFueSBzdHlsZXMgdGhhdCB3aWxsIGJyZWFrIGhlaWdodCB0cmFuc2l0aW9ucy5cbiAqXG4gKiBAcGFyYW0ge0hUTUxFbGVtZW50fSBjb250ZW50IEhvdWRpbmkgY29udGVudCB0aGF0IHdpbGwgcmVjZWl2ZSBjb25jcmV0ZSBoZWlnaHRzXG4gKiBAcGFyYW0ge0hUTUxFbGVtZW50fSBbdG9nZ2xlXSBPcHRpb25hbCBIb3VkaW5pIHRvZ2dsZSBlbGVtZW50IGFzc29jaWF0ZWQgdG8gdGhlIGNvbnRlbnRcbiAqIEBzZWUge0BsaW5rIGh0dHBzOi8vY3NzLXRyaWNrcy5jb20vdXNpbmctY3NzLXRyYW5zaXRpb25zLWF1dG8tZGltZW5zaW9ucy8jYXJ0aWNsZS1oZWFkZXItaWQtNX1cbiAqL1xuZXhwb3J0IGZ1bmN0aW9uIGV4cGFuZChjb250ZW50LCB0b2dnbGUpIHtcblx0Y29uc3QgdGFyZ2V0SGVpZ2h0ID0gY29udGVudC5zY3JvbGxIZWlnaHQ7XG5cdGNvbnRlbnQuc3R5bGUuaGVpZ2h0ID0gYCR7dGFyZ2V0SGVpZ2h0fXB4YDtcblxuXHRjb250ZW50LmFkZEV2ZW50TGlzdGVuZXIoJ3RyYW5zaXRpb25lbmQnLCBmdW5jdGlvbiB0cmFuc2l0aW9uSGFuZGxlcigpIHtcblx0XHQvLyBSZW1vdmUgdGhlIHRyYW5zaXRpb24gbGlzdGVuZXIgc28gaXQgb25seSBnZXRzIHRyaWdnZXJlZCBvbmNlXG5cdFx0Y29udGVudC5yZW1vdmVFdmVudExpc3RlbmVyKCd0cmFuc2l0aW9uZW5kJywgdHJhbnNpdGlvbkhhbmRsZXIpO1xuXHRcdC8vIFJlLWVuYWJsZSB0aGUgcmVndWxhciBjb250ZW50IGZsb3cgYnkgcmVtb3ZpbmcgdGhlIGNvbmNyZXRlIGhlaWdodFxuXHRcdGNvbnRlbnQuc3R5bGUucmVtb3ZlUHJvcGVydHkoJ2hlaWdodCcpO1xuXHRcdGRpc3BhdGNoRXZlbnQoJ29wZW5lZEFuaW1hdGlvbi5ob3VkaW5pJywgY29udGVudCwgeyB0b2dnbGUgfSk7XG5cdH0pO1xufVxuXG4vKipcbiAqIEFkZCBoZWlnaHQgdHJhbnNpdGlvbiBzdXBwb3J0IHdoZW4gKipjb2xsYXBzaW5nKiogSG91ZGluaSBlbGVtZW50cy5cbiAqIFRyYW5zaXRpb25zIHNob3VsZCBiZSBoYW5kbGVkIGJ5IENTUywgdGhlIGZ1bmN0aW9uIHdpbGwganVzdCBnaXZlIHRoZSBjb250ZW50XG4gKiBjb25jcmV0ZSBoZWlnaHQgdmFsdWVzIHNpbmNlIHRyYW5zaXRpb25pbmcgZnJvbSBoZWlnaHQ6IGF1dG8gaXMgbm90IHBvc3NpYmxlLlxuICogTWFrZSBzdXJlIGBjb250ZW50YCBkb2Vzbid0IHJlY2VpdmUgSG91ZGluaSdzIHN0YW5kYXJkICdjb2xsYXBzZScgY2xhc3MsIHNpbmNlIGl0IHdpbGxcbiAqIHNldCBtYW55IHN0eWxlcyB0aGF0IHdpbGwgYnJlYWsgaGVpZ2h0IHRyYW5zaXRpb25zLlxuICpcbiAqIEBwYXJhbSB7SFRNTEVsZW1lbnR9IGNvbnRlbnQgSG91ZGluaSBjb250ZW50IHRoYXQgd2lsbCByZWNlaXZlIGNvbmNyZXRlIGhlaWdodHNcbiAqIEBwYXJhbSB7SFRNTEVsZW1lbnR9IFt0b2dnbGVdIE9wdGlvbmFsIEhvdWRpbmkgdG9nZ2xlIGVsZW1lbnQgYXNzb2NpYXRlZCB0byB0aGUgY29udGVudFxuICogQHNlZSB7QGxpbmsgaHR0cHM6Ly9jc3MtdHJpY2tzLmNvbS91c2luZy1jc3MtdHJhbnNpdGlvbnMtYXV0by1kaW1lbnNpb25zLyNhcnRpY2xlLWhlYWRlci1pZC01fVxuICovXG5leHBvcnQgZnVuY3Rpb24gY29sbGFwc2UoY29udGVudCwgdG9nZ2xlKSB7XG5cdGNvbnN0IHRhcmdldEhlaWdodCA9IGNvbnRlbnQuc2Nyb2xsSGVpZ2h0O1xuXHRjb25zdCBjb250ZW50VHJhbnNpdGlvbiA9IGNvbnRlbnQuc3R5bGUudHJhbnNpdGlvbjtcblx0Y29udGVudC5zdHlsZS50cmFuc2l0aW9uID0gJyc7XG5cblx0Ly8gT24gdGhlIG5leHQgZnJhbWUgKGFzIHNvb24gYXMgdGhlIHByZXZpb3VzIHN0eWxlIGNoYW5nZSBoYXMgdGFrZW4gZWZmZWN0KSxcblx0Ly8gZXhwbGljaXRseSBzZXQgdGhlIGNvbnRlbnQncyBoZWlnaHQgdG8gaXRzIGN1cnJlbnQgcGl4ZWwgaGVpZ2h0LCBzbyB3ZVxuXHQvLyBhcmVuJ3QgdHJhbnNpdGlvbmluZyBvdXQgb2YgJ2F1dG8nXG5cdHJlcXVlc3RBbmltYXRpb25GcmFtZSgoKSA9PiB7XG5cdFx0Y29udGVudC5zdHlsZS5oZWlnaHQgPSBgJHt0YXJnZXRIZWlnaHR9cHhgO1xuXHRcdGNvbnRlbnQuc3R5bGUudHJhbnNpdGlvbiA9IGNvbnRlbnRUcmFuc2l0aW9uO1xuXG5cdFx0Ly8gRmluYWxseSB0cmFuc2l0aW9uIHRvIGhlaWdodDogMFxuXHRcdHJlcXVlc3RBbmltYXRpb25GcmFtZSgoKSA9PiB7XG5cdFx0XHRjb250ZW50LnN0eWxlLmhlaWdodCA9ICcwcHgnO1xuXHRcdFx0ZGlzcGF0Y2hFdmVudCgnY2xvc2VkQW5pbWF0aW9uLmhvdWRpbmknLCBjb250ZW50LCB7IHRvZ2dsZSB9KTtcblx0XHR9KTtcblx0fSk7XG59XG5cbi8qKlxuICogQWRkIGV2ZW50IGxpc3RlbmVycyB0byBhbmltYXRlIEhvdWRpbmkgZXhwYW5kL2NvbGxhcHNlIGFjdGlvbnNcbiAqIEBwYXJhbSAge1N0cmluZ30gb3B0aW9ucy5jb250ZW50Q2xhc3MgQ2xhc3Mgb2YgdGhlIGFuaW1hdGVkIEhvdWRpbmkgY29sbGFwc2libGVzXG4gKiBAcGFyYW0gIHtTdHJpbmd9IG9wdGlvbnMuYWN0aXZlQ2xhc3MgIENsYXNzIHRoYXQgaW5kaWNhdGVzIGEgY29sbGFwc2libGUgaXMgZXhwYW5kZWRcbiAqL1xuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gaW5pdEFuaW1hdGlvbnMoe1xuXHRjb250ZW50Q2xhc3MgPSAnY29sbGFwc2UtYW5pbWF0ZWQnLFxuXHRhY3RpdmVDbGFzcyA9ICdhY3RpdmUnLFxufSA9IHt9KSB7XG5cdC8vIE5vdGljZSB3ZSBhcmUgYXR0YWNoaW5nIHRvIHRoZSBldmVudHMgZmlyZWQgYmVmb3JlIEhvdWRpbmkgZG9lcyB0aGUgY2xhc3Mgc3dpdGNoaW5nXG5cdGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ29wZW4uaG91ZGluaScsIGV2ZW50ID0+IHtcblx0XHRjb25zdCBjb250ZW50ID0gZXZlbnQudGFyZ2V0O1xuXHRcdGlmIChjb250ZW50LmNsYXNzTGlzdC5jb250YWlucyhjb250ZW50Q2xhc3MpKSB7XG5cdFx0XHRleHBhbmQoY29udGVudCwgZXZlbnQuZGV0YWlsLnRvZ2dsZSk7XG5cdFx0fVxuXHR9KTtcblxuXHRkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdjbG9zZS5ob3VkaW5pJywgZXZlbnQgPT4ge1xuXHRcdGNvbnN0IGNsYXNzTGlzdCA9IGV2ZW50LnRhcmdldC5jbGFzc0xpc3Q7XG5cdFx0aWYgKGNsYXNzTGlzdC5jb250YWlucyhjb250ZW50Q2xhc3MpICYmIGNsYXNzTGlzdC5jb250YWlucyhhY3RpdmVDbGFzcykpIHtcblx0XHRcdGNvbGxhcHNlKGV2ZW50LnRhcmdldCwgZXZlbnQuZGV0YWlsLnRvZ2dsZSk7XG5cdFx0fVxuXHR9KTtcblxuXHQvLyBDb2xsYXBzZSBhbGwgY29udGVudHMgbm90IG1hcmtlZCBhcyBpbml0aWFsbHkgYWN0aXZlXG5cdC8vIFRoaXMgZ2l2ZXMgdXMgYSBjb25zaXN0ZW50IGFuaW1hdGlvbiBhbmQgRE9NIHN0YXRlXG5cdGNvbnN0IGFuaW1hdGVkQ29udGVudHMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKGAuJHtjb250ZW50Q2xhc3N9YCk7XG5cdFtdLmZvckVhY2guY2FsbChhbmltYXRlZENvbnRlbnRzLCBjb250ZW50ID0+IHtcblx0XHRpZiAoIWNvbnRlbnQuY2xhc3NMaXN0LmNvbnRhaW5zKGFjdGl2ZUNsYXNzKSkgY29sbGFwc2UoY29udGVudCk7XG5cdH0pO1xufVxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2FuaW1hdGlvbnMuanMiLCJpbXBvcnQgaG91ZGluaSBmcm9tICdIb3VkaW5pJztcblxuLyoqXG4gKiBBdHRhY2ggZXZlbnQgbGlzdGVuZXJzIHRvIGNsb3NlIEhvdWRpbmkgY29udGVudHMuXG4gKiBDb21wYXRpYmxlIHdpdGggYW55IGVsZW1lbnQgd2l0aCBhIGRhdGEtY2xvc2UgYXR0cmlidXRlIHdpdGggYW4gSUQgYXMgdmFsdWUuXG4gKiBAZXhhbXBsZSA8YnV0dG9uIGRhdGEtY2xvc2U9XCIjaWQtb2YtY29udGVudFwiPkNsb3NlPC9idXR0b24+XG4gKi9cbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIGluaXRDbG9zZUJ1dHRvbnMoKSB7XG5cdGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgZXZlbnQgPT4ge1xuXHRcdGNvbnN0IHRhcmdldElkID0gZXZlbnQudGFyZ2V0LmRhdGFzZXQuY2xvc2U7XG5cdFx0aWYgKHRhcmdldElkKSB7XG5cdFx0XHRjb25zdCB0b2dnbGUgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGBbaHJlZj1cIiR7dGFyZ2V0SWR9XCJdYCk7XG5cdFx0XHRob3VkaW5pLmNsb3NlQ29udGVudCh0YXJnZXRJZCwgdG9nZ2xlKTtcblx0XHR9XG5cdH0pO1xufVxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2Nsb3NlLWJ1dHRvbnMuanMiLCJpbXBvcnQgaG91ZGluaSBmcm9tICdIb3VkaW5pJztcblxuLyoqXG4gKiBBdHRhY2ggZXZlbnQgbGlzdGVuZXJzIHRvIGNyZWF0ZSBIb3VkaW5pLWJhc2VkIG1vZGFscy5cbiAqXG4gKiBAcGFyYW0ge1N0cmluZ30gb3B0aW9ucy5tb2RhbENsYXNzICBDbGFzcyBuYW1lIHRoYXQgaWRlbnRpZmllcyBhbGwgbW9kYWxzXG4gKiBAcGFyYW0ge1N0cmluZ30gb3B0aW9ucy5hY3RpdmVDbGFzcyBDbGFzcyBuYW1lIHRoYXQgaW5kaWNhdGVzIGEgbW9kYWwgaXMgYSBhY3RpdmVcbiAqL1xuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gaW5pdE1vZGFscyh7XG5cdG1vZGFsQ2xhc3MgPSAnaG91ZGluaS1tb2RhbCcsXG5cdGFjdGl2ZUNsYXNzID0gJ2FjdGl2ZScsXG59ID0ge30pIHtcblx0Y29uc3QgYm9keSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ2JvZHknKTtcblx0Y29uc3QgaXNNb2RhbCA9IGVsID0+IGVsLmNsYXNzTGlzdC5jb250YWlucyhtb2RhbENsYXNzKTtcblxuXHQvLyBXaGVuIGEgbW9kYWwgaXMgb3BlbmVkLi4uXG5cdGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ29wZW5lZC5ob3VkaW5pJywgZXZlbnQgPT4ge1xuXHRcdC8vIFJlbW92ZSB0aGUgPGJvZHk+IHNjcm9sbGJhclxuXHRcdGlmIChpc01vZGFsKGV2ZW50LnRhcmdldCkpIGJvZHkuc3R5bGUub3ZlcmZsb3dZID0gJ2hpZGRlbic7XG5cdH0pO1xuXG5cdC8vIFdoZW4gYSBtb2RhbCBpcyBjbG9zZWQuLi5cblx0ZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignY2xvc2VkLmhvdWRpbmknLCBldmVudCA9PiB7XG5cdFx0aWYgKGlzTW9kYWwoZXZlbnQudGFyZ2V0KSkge1xuXHRcdFx0Ly8gUmV0dXJuIHRoZSA8Ym9keT4gdG8gaXRzIG5vcm1hbCBzY3JvbGxiYXIgc3RhdGVcblx0XHRcdGJvZHkuc3R5bGUucmVtb3ZlUHJvcGVydHkoJ292ZXJmbG93LXknKTtcblx0XHRcdC8vIEdpdmUgZm9jdXMgYmFjayB0byB0aGUgdG9nZ2xlIHRoYXQgdHJpZ2dlcmVkIHRoZSBtb2RhbFxuXHRcdFx0aWYgKGV2ZW50LmRldGFpbC50b2dnbGUpIGV2ZW50LmRldGFpbC50b2dnbGUuZm9jdXMoKTtcblx0XHR9XG5cdH0pO1xuXG5cdC8vIENsb3NlIHRoZSBtb2RhbCB3aGVuIHRoZSBtb2RhbCBvdmVybGF5IGlzIGNsaWNrZWRcblx0ZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBldmVudCA9PiB7XG5cdFx0Y29uc3QgY29udGVudCA9IGV2ZW50LnRhcmdldDtcblxuXHRcdGlmIChpc01vZGFsKGNvbnRlbnQpKSB7XG5cdFx0XHRjb25zdCB0b2dnbGVFbCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoYFtocmVmPVwiIyR7Y29udGVudC5pZH1cIl1gKTtcblx0XHRcdGhvdWRpbmkuY2xvc2VDb250ZW50KGNvbnRlbnQuaWQsIHRvZ2dsZUVsKTtcblx0XHR9XG5cdH0pO1xuXG5cdC8vIENsb3NlIHRoZSBhY3RpdmUgbW9kYWwgb24gRVNDXG5cdGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ2tleWRvd24nLCBlID0+IHtcblx0XHRpZiAoZS5rZXlDb2RlID09PSAyNykge1xuXHRcdFx0Y29uc3Qgb3Blbk1vZGFsID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihgLiR7bW9kYWxDbGFzc30uJHthY3RpdmVDbGFzc31gKTtcblx0XHRcdGlmIChvcGVuTW9kYWwpIHtcblx0XHRcdFx0Y29uc3QgdG9nZ2xlRWwgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGBbaHJlZj1cIiMke29wZW5Nb2RhbC5pZH1cIl1gKTtcblx0XHRcdFx0aG91ZGluaS5jbG9zZUNvbnRlbnQob3Blbk1vZGFsLmlkLCB0b2dnbGVFbCk7XG5cdFx0XHR9XG5cdFx0fVxuXHR9KTtcbn1cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9tb2RhbHMuanMiLCJpbXBvcnQgaG91ZGluaSBmcm9tICdIb3VkaW5pJztcblxuLyoqXG4gKiBHZW5lcmF0ZSBhIFlvdVR1YmUgb3IgVmltZW8gZW1iZWQgY29kZSBmcm9tIGEgVVJMLlxuICogVGhlIHJlc3VsdGluZyBpZnJhbWUgY29kZSB3aWxsIGhhdmUgYXV0b3BsYXkgZW5hYmxlZC5cbiAqIEBwYXJhbSAge1N0cmluZ30gdXJsIFRoZSB1cmwgb2YgYSBZb3VUdWJlIG9yIFZpbWVvIHZpZGVvXG4gKiBAcmV0dXJuIHtTdHJpbmd9ICAgICBUaGUgZW1iZWQgY29kZSBmb3IgWW91VHViZSBvciBWaW1lb1xuICovXG5leHBvcnQgZnVuY3Rpb24gZ2V0RW1iZWRDb2RlKHVybCkge1xuXHRjb25zdCB2aW1lb1JFID0gLyg/Omh0dHBzPzpcXC9cXC8pPyg/Ond3d1xcLik/KD86dmltZW9cXC5jb20pXFwvKFteLz9dKykvZztcblx0Y29uc3QgeW91dHViZVJFID0gLyg/Omh0dHBzPzpcXC9cXC8pPyg/Ond3d1xcLik/KD86eW91dHViZVxcLmNvbXx5b3V0dVxcLmJlKVxcLyg/OndhdGNoXFw/dj0pPyhbXi8mXSspL2c7XG5cdGxldCBtYXRjaDtcblxuXHRtYXRjaCA9IHZpbWVvUkUuZXhlYyh1cmwpO1xuXHRpZiAobWF0Y2gpIHtcblx0XHRyZXR1cm4gYDxpZnJhbWUgd2lkdGg9XCI4NDBcIiBoZWlnaHQ9XCI0NzBcIiBhbGxvd2Z1bGxzY3JlZW5cblx0XHRcdHNyYz1cImh0dHBzOi8vcGxheWVyLnZpbWVvLmNvbS92aWRlby8ke21hdGNoWzFdfT9hdXRvcGxheT0xXCI+PC9pZnJhbWU+YDtcblx0fVxuXG5cdG1hdGNoID0geW91dHViZVJFLmV4ZWModXJsKTtcblx0aWYgKG1hdGNoKSB7XG5cdFx0cmV0dXJuIGA8aWZyYW1lIHdpZHRoPVwiODQwXCIgaGVpZ2h0PVwiNDcwXCIgYWxsb3dmdWxsc2NyZWVuXG5cdFx0XHRzcmM9XCJodHRwczovL3d3dy55b3V0dWJlLmNvbS9lbWJlZC8ke21hdGNoWzFdfT9hdXRvcGxheT0xXCI+PC9pZnJhbWU+YDtcblx0fVxuXG5cdHJldHVybiAnJztcbn1cblxuLyoqXG4gKiBBdHRhY2ggZXZlbnQgbGlzdGVuZXJzIHJlcXVpcmVkIHRvIGNyZWF0ZSBhbmQgc2hvdyB2aWRlbyBtb2RhbHMuXG4gKlxuICogQHBhcmFtICB7U3RyaW5nfSBvcHRpb25zLm1vZGFsQ2xhc3MgICAgIENsYXNzIG5hbWUgYXBwbGllZCB0byBhbGwgbW9kYWxzXG4gKiBAcGFyYW0gIHtTdHJpbmd9IG9wdGlvbnMudmlkZW9DbGFzcyAgICAgQ2xhc3MgbmFtZSBhcHBsaWVkIHRvIGFsbCB2aWRlbyBtb2RhbHNcbiAqIEBwYXJhbSAge1N0cmluZ30gb3B0aW9ucy5idXR0b25TZWxlY3RvciBTZWxlY3RvciBvZiBhbGwgZWxlbWVudHMgdGhhdCBmaXJlIHZpZGVvIG1vZGFsc1xuICogQHBhcmFtICB7U3RyaW5nfSBvcHRpb25zLm1vZGFsVGVtcGxhdGVcbiAqICAgICAgICAgVmlkZW8gbW9kYWwgbWFya3VwLiBge3tpZnJhbWV9fWAgd2lsbCBiZSByZXBsYWNlZCB3aXRoIHRoZSBhY3R1YWwgZW1iZWQgY29kZS5cbiAqL1xuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gaW5pdFZpZGVvcyh7XG5cdG1vZGFsQ2xhc3MgPSAnaG91ZGluaS1tb2RhbCcsXG5cdHZpZGVvQ2xhc3MgPSAnaG91ZGluaS12aWRlby1tb2RhbCcsXG5cdGJ1dHRvblNlbGVjdG9yID0gJ1tkYXRhLXZpZGVvXScsXG5cdG1vZGFsVGVtcGxhdGUgPSAnPGRpdiBjbGFzcz1cImhvdWRpbmktbW9kYWwgaG91ZGluaS12aWRlby1tb2RhbFwiPnt7aWZyYW1lfX08L2Rpdj4nLFxufSA9IHt9KSB7XG5cdGNvbnN0IHZpZGVvQnRucyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoYnV0dG9uU2VsZWN0b3IpO1xuXHRsZXQgdmlkZW9Nb2RhbDtcblxuXHQvLyBPcGVuIHZpZGVvIG1vZGFscyB3aGVuIHZpZGVvIGJ1dHRvbnMgYXJlIGNsaWNrZWRcblx0W10uZm9yRWFjaC5jYWxsKHZpZGVvQnRucywgdmlkZW9CdG4gPT4ge1xuXHRcdHZpZGVvQnRuLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgZXZlbnQgPT4ge1xuXHRcdFx0ZXZlbnQucHJldmVudERlZmF1bHQoKTtcblx0XHRcdGNvbnN0IGVtYmVkQ29kZSA9IGdldEVtYmVkQ29kZSh2aWRlb0J0bi5ocmVmKTtcblx0XHRcdGNvbnN0IHRlbXBOb2RlID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG5cdFx0XHR0ZW1wTm9kZS5pbm5lckhUTUwgPSBtb2RhbFRlbXBsYXRlLnJlcGxhY2UoJ3t7aWZyYW1lfX0nLCBlbWJlZENvZGUpO1xuXHRcdFx0dmlkZW9Nb2RhbCA9IHRlbXBOb2RlLmZpcnN0RWxlbWVudENoaWxkO1xuXHRcdFx0dmlkZW9Nb2RhbC5pZCA9ICdob3VkaW5pLXZpZGVvLW1vZGFsJztcblx0XHRcdGRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQodmlkZW9Nb2RhbCk7XG5cdFx0XHRzZXRUaW1lb3V0KCgpID0+IHtcblx0XHRcdFx0aG91ZGluaS5vcGVuQ29udGVudCh2aWRlb01vZGFsLmlkLCB2aWRlb0J0bik7XG5cdFx0XHR9LCAxMCk7XG5cdFx0fSk7XG5cdH0pO1xuXG5cdC8vIERlbGV0ZSB0aGUgdmlkZW9Nb2RhbCBub2RlIHdoZW4gY2xvc2VkXG5cdGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ2Nsb3NlZC5ob3VkaW5pJywgZXZlbnQgPT4ge1xuXHRcdGNvbnN0IGNsYXNzZXMgPSBldmVudC50YXJnZXQuY2xhc3NMaXN0O1xuXG5cdFx0aWYgKGNsYXNzZXMuY29udGFpbnMobW9kYWxDbGFzcykgJiYgY2xhc3Nlcy5jb250YWlucyh2aWRlb0NsYXNzKSkge1xuXHRcdFx0dmlkZW9Nb2RhbCA9IGV2ZW50LnRhcmdldDtcblx0XHRcdHZpZGVvTW9kYWwucGFyZW50Tm9kZS5yZW1vdmVDaGlsZCh2aWRlb01vZGFsKTtcblx0XHR9XG5cdH0pO1xufVxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL3ZpZGVvcy5qcyIsImV4cG9ydCB7IGRlZmF1bHQgYXMgaW5pdEFuaW1hdGlvbnMgfSBmcm9tICcuL2FuaW1hdGlvbnMnO1xuZXhwb3J0IHsgZGVmYXVsdCBhcyBpbml0Q2xvc2VCdXR0b25zIH0gZnJvbSAnLi9jbG9zZS1idXR0b25zJztcbmV4cG9ydCB7IGRlZmF1bHQgYXMgaW5pdE1vZGFscyB9IGZyb20gJy4vbW9kYWxzJztcbmV4cG9ydCB7IGRlZmF1bHQgYXMgaW5pdFZpZGVvcyB9IGZyb20gJy4vdmlkZW9zJztcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9pbmRleC5qcyJdLCJzb3VyY2VSb290IjoiIn0=