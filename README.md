# Houdini Extras

A collection of handy addons for the [Houdini JS library].

## IMPORTANT

As of now, these utilities require a patched version of Houdini that supports an [event interface], which means you'll need to install Houdini from my fork:

```bash
npm install --save github:jerivas/houdini#26645ae8c28a230d3cfc29719530de005dbcefd6
```

This will add "Houdini" to your `package.json`.

## Installation

Install from git via npm. The package will be saved as `houdini-extras`.

```bash
# Latest version
npm install --save git+https://gitlab.com/jerivas/houdini-extras.git

# Specific version (add tag at the end)
npm install --save git+https://gitlab.com/jerivas/houdini-extras.git#v1.1.0
```

## Usage

**Before using any of the extras, make sure you've initialized Houdini in your document: `Houdini.init()`.**

The module is UMD compatible, so you can `require` or `import` it in your code. For example, using ES6 modules:

```javascript
import { initAnimations, initModals, initVideos, initCloseButtons } from 'houdini-extras';
```

You can also use the bundle directly from the `dist` folder:

```html
<script src="dist/houdini-extras.min.js"></script>
<script>
    houdiniExtras.initAnimations();
    houdiniExtras.initModals();
    houdiniExtras.initVideos();
    houdiniExtras.initCloseButtons();
</script>
```

## Browser support

Should have the same compatibility as Houdini itself. The only gotcha is the usage of `CustomEvent`, which IE doesn't support. You can easily add a [CustomEvent polyfill]. There's also a pre-packaged [ponyfill on npm].

## What's included


### Animations

Add smooth height animations when collapsing / expanding elements with Houdini. Just change your content elements from  `class="collapse"` to `class="collapse-animated"`. The style rules you'll need are:

```css
.collapse-animated {
    overflow: hidden;
    transition: height 500ms; /* Use whatever duration you like */
}
```

```javascript
// Showing default configuration values
houdiniExtras.initAnimations({
    contentClass: 'collapse-animated',
    activeClass: 'active',
});
```

Events will be dispatched after the animation ends (for both the collapse and expand animations).

- Event names: `closedAnimation.houdini` and `openedAnimation.houdini`.
- The value of `event.target` will be the DOM node that was just animated.
- The value of `event.detail.toggle` will be the DOM node that toggled the action (optional).

### Modals

The Modal addon module allows you to convert regular Houdini collapsibles into modal windows.

- The `<body>` scrollbar is disabled when the modal is open
- Focus is returned to the element that opened the modal once it's closed
- The modal is closed when the overlay is clicked
- The modal is closed when the ESC key is pressed

Simply add the class `houdini-modal` to a Houdini collapsible and you can now open/close it as any Houdini modal. The styling (full screen, dark overlay, etc) is left to you, but `.houdini-modal` should be hidden and become visible only when the `active` class is added.

```html
<div id="modal" class="houdini-modal">
   (modal content here)
</div>

<a href="#modal" data-toggle>Show modal!</a>
```

```javascript
// Showing default configuration values
houdiniExtras.initModals({
    modalClass: 'houdini-modal',
    activeClass: 'active',
});
```

### Videos

**Requires the Modals addon to be initialized first.**

You can also auto-embed videos in modals just by providing their URL and adding a `data-video` attribute:

```html
<a href="https://www.youtube.com/watch?v=c8aFcHFu8QM" data-video>Watch video</a>
```

```javascript
// Showing default configuration values
houdiniExtras.initVideos({
    modalClass: 'houdini-modal',
    videoClass: 'houdini-video-modal',
    buttonSelector: '[data-video]',
    modalTemplate: '<div class="houdini-modal houdini-video-modal">{{iframe}}</div>',
});
```

It's important to note that you can pass any HTML string to the `modalTemplate` option, just keep in mind:

- It should incorporate the classes from the `modalClass` and `videoClass` options.
- It should include the string `{{iframe}}` to inject the video iframe.

### Close buttons

This addon provides dedicated buttons to close Houdini collapsibles. You can close any Houdini element by its id in a `data-close` attribute:

```html
<button data-close="#element-id"></button>
```

```javascript
// This addon doesn't take any configuration
houdiniExtras.initCloseButtons();
```

[Houdini JS library]: https://github.com/cferdinandi/houdini
[event interface]: https://github.com/cferdinandi/houdini/pull/60
[CustomEvent polyfill]: https://developer.mozilla.org/en-US/docs/Web/API/CustomEvent/CustomEvent#Polyfill
[ponyfill on npm]: https://www.npmjs.com/package/customevent
