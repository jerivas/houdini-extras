import houdini from 'Houdini';

/**
 * Attach event listeners to create Houdini-based modals.
 *
 * @param {String} options.modalClass  Class name that identifies all modals
 * @param {String} options.activeClass Class name that indicates a modal is a active
 */
export default function initModals({
	modalClass = 'houdini-modal',
	activeClass = 'active',
} = {}) {
	const body = document.querySelector('body');
	const isModal = el => el.classList.contains(modalClass);

	// When a modal is opened...
	document.addEventListener('opened.houdini', event => {
		// Remove the <body> scrollbar
		if (isModal(event.target)) body.style.overflowY = 'hidden';
	});

	// When a modal is closed...
	document.addEventListener('closed.houdini', event => {
		if (isModal(event.target)) {
			// Return the <body> to its normal scrollbar state
			body.style.removeProperty('overflow-y');
			// Give focus back to the toggle that triggered the modal
			if (event.detail.toggle) event.detail.toggle.focus();
		}
	});

	// Close the modal when the modal overlay is clicked
	document.addEventListener('click', event => {
		const content = event.target;

		if (isModal(content)) {
			const toggleEl = document.querySelector(`[href="#${content.id}"]`);
			houdini.closeContent(content.id, toggleEl);
		}
	});

	// Close the active modal on ESC
	document.addEventListener('keydown', e => {
		if (e.keyCode === 27) {
			const openModal = document.querySelector(`.${modalClass}.${activeClass}`);
			if (openModal) {
				const toggleEl = document.querySelector(`[href="#${openModal.id}"]`);
				houdini.closeContent(openModal.id, toggleEl);
			}
		}
	});
}
