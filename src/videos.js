import houdini from 'Houdini';

/**
 * Generate a YouTube or Vimeo embed code from a URL.
 * The resulting iframe code will have autoplay enabled.
 * @param  {String} url The url of a YouTube or Vimeo video
 * @return {String}     The embed code for YouTube or Vimeo
 */
export function getEmbedCode(url) {
	const vimeoRE = /(?:https?:\/\/)?(?:www\.)?(?:vimeo\.com)\/([^/?]+)/g;
	const youtubeRE = /(?:https?:\/\/)?(?:www\.)?(?:youtube\.com|youtu\.be)\/(?:watch\?v=)?([^/&]+)/g;
	let match;

	match = vimeoRE.exec(url);
	if (match) {
		return `<iframe width="840" height="470" allowfullscreen
			src="https://player.vimeo.com/video/${match[1]}?autoplay=1"></iframe>`;
	}

	match = youtubeRE.exec(url);
	if (match) {
		return `<iframe width="840" height="470" allowfullscreen
			src="https://www.youtube.com/embed/${match[1]}?autoplay=1"></iframe>`;
	}

	return '';
}

/**
 * Attach event listeners required to create and show video modals.
 *
 * @param  {String} options.modalClass     Class name applied to all modals
 * @param  {String} options.videoClass     Class name applied to all video modals
 * @param  {String} options.buttonSelector Selector of all elements that fire video modals
 * @param  {String} options.modalTemplate
 *         Video modal markup. `{{iframe}}` will be replaced with the actual embed code.
 */
export default function initVideos({
	modalClass = 'houdini-modal',
	videoClass = 'houdini-video-modal',
	buttonSelector = '[data-video]',
	modalTemplate = '<div class="houdini-modal houdini-video-modal">{{iframe}}</div>',
} = {}) {
	const videoBtns = document.querySelectorAll(buttonSelector);
	let videoModal;

	// Open video modals when video buttons are clicked
	[].forEach.call(videoBtns, videoBtn => {
		videoBtn.addEventListener('click', event => {
			event.preventDefault();
			const embedCode = getEmbedCode(videoBtn.href);
			const tempNode = document.createElement('div');
			tempNode.innerHTML = modalTemplate.replace('{{iframe}}', embedCode);
			videoModal = tempNode.firstElementChild;
			videoModal.id = 'houdini-video-modal';
			document.body.appendChild(videoModal);
			setTimeout(() => {
				houdini.openContent(videoModal.id, videoBtn);
			}, 10);
		});
	});

	// Delete the videoModal node when closed
	document.addEventListener('closed.houdini', event => {
		const classes = event.target.classList;

		if (classes.contains(modalClass) && classes.contains(videoClass)) {
			videoModal = event.target;
			videoModal.parentNode.removeChild(videoModal);
		}
	});
}
