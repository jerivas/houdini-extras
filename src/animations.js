 /**
 * Instantiate and dispatch a custom event
 * @param  {String} name Name of the custom event
 * @param  {Element} target DOM element that will be available as event.target
 * @param  {Object} detail Object that will be available as event.detail
 */
function dispatchEvent(name, target, detail) {
	// IE doesn't support CustomEvent
	if (typeof window.CustomEvent !== 'function') return;
	const event = new CustomEvent(name, {
		detail,
		bubbles: true,
		cancelable: true,
	});
	target.dispatchEvent(event);
}

/**
 * Add height transition support when **expanding** Houdini elements.
 * Transitions should be handled by CSS, the function will just give the content
 * concrete height values since transitioning into height: auto is not possible.
 * Make sure `content` doesn't receive Houdini's standard 'collapse' class, since it will
 * set many styles that will break height transitions.
 *
 * @param {HTMLElement} content Houdini content that will receive concrete heights
 * @param {HTMLElement} [toggle] Optional Houdini toggle element associated to the content
 * @see {@link https://css-tricks.com/using-css-transitions-auto-dimensions/#article-header-id-5}
 */
export function expand(content, toggle) {
	const targetHeight = content.scrollHeight;
	content.style.height = `${targetHeight}px`;

	content.addEventListener('transitionend', function transitionHandler() {
		// Remove the transition listener so it only gets triggered once
		content.removeEventListener('transitionend', transitionHandler);
		// Re-enable the regular content flow by removing the concrete height
		content.style.removeProperty('height');
		dispatchEvent('openedAnimation.houdini', content, { toggle });
	});
}

/**
 * Add height transition support when **collapsing** Houdini elements.
 * Transitions should be handled by CSS, the function will just give the content
 * concrete height values since transitioning from height: auto is not possible.
 * Make sure `content` doesn't receive Houdini's standard 'collapse' class, since it will
 * set many styles that will break height transitions.
 *
 * @param {HTMLElement} content Houdini content that will receive concrete heights
 * @param {HTMLElement} [toggle] Optional Houdini toggle element associated to the content
 * @see {@link https://css-tricks.com/using-css-transitions-auto-dimensions/#article-header-id-5}
 */
export function collapse(content, toggle) {
	const targetHeight = content.scrollHeight;
	const contentTransition = content.style.transition;
	content.style.transition = '';

	// On the next frame (as soon as the previous style change has taken effect),
	// explicitly set the content's height to its current pixel height, so we
	// aren't transitioning out of 'auto'
	requestAnimationFrame(() => {
		content.style.height = `${targetHeight}px`;
		content.style.transition = contentTransition;

		// Finally transition to height: 0
		requestAnimationFrame(() => {
			content.style.height = '0px';
			dispatchEvent('closedAnimation.houdini', content, { toggle });
		});
	});
}

/**
 * Add event listeners to animate Houdini expand/collapse actions
 * @param  {String} options.contentClass Class of the animated Houdini collapsibles
 * @param  {String} options.activeClass  Class that indicates a collapsible is expanded
 */
export default function initAnimations({
	contentClass = 'collapse-animated',
	activeClass = 'active',
} = {}) {
	// Notice we are attaching to the events fired before Houdini does the class switching
	document.addEventListener('open.houdini', event => {
		const content = event.target;
		if (content.classList.contains(contentClass)) {
			expand(content, event.detail.toggle);
		}
	});

	document.addEventListener('close.houdini', event => {
		const classList = event.target.classList;
		if (classList.contains(contentClass) && classList.contains(activeClass)) {
			collapse(event.target, event.detail.toggle);
		}
	});

	// Collapse all contents not marked as initially active
	// This gives us a consistent animation and DOM state
	const animatedContents = document.querySelectorAll(`.${contentClass}`);
	[].forEach.call(animatedContents, content => {
		if (!content.classList.contains(activeClass)) collapse(content);
	});
}
