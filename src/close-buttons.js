import houdini from 'Houdini';

/**
 * Attach event listeners to close Houdini contents.
 * Compatible with any element with a data-close attribute with an ID as value.
 * @example <button data-close="#id-of-content">Close</button>
 */
export default function initCloseButtons() {
	document.addEventListener('click', event => {
		const targetId = event.target.dataset.close;
		if (targetId) {
			const toggle = document.querySelector(`[href="${targetId}"]`);
			houdini.closeContent(targetId, toggle);
		}
	});
}
